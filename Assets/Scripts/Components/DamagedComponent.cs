using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct DamagedComponent
{
    public int Damage;
}
