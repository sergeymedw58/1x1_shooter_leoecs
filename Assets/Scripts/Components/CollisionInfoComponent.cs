﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct CollisionInfoComponent
{
    public GameObject Other;
    public Bullet Bullet;
    public Vector3 Normal;
}