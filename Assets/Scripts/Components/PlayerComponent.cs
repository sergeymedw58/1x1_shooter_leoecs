using UnityEngine.AI;
using UnityEngine;

public struct PlayerComponent
{
    public NavMeshAgent Agent;
    public Transform Transform;
    public Transform ShootPoint;
    public int Score;
    public Vector3 StartPosition;
}
