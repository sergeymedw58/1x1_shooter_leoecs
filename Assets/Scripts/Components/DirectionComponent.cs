using UnityEngine;

public struct MovementComponent
{
    public Vector3 Direction;
    public float Speed;
}
