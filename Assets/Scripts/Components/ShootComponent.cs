using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ShootComponent 
{
    public Bullet Bullet;
    public Vector3 Direction;
    public Vector3 BulletSpawnPoint;
    public bool IsShooting;
}
