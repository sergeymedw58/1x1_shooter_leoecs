using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public struct EnemyComponent
{
    public List<Vector3> WayPoints;
    public NavMeshAgent Agent;
    public Transform Transform;
    public Transform ShootPoint;
    public bool CanAiming;
    public bool IsOnTheWay;
    public int Score;
    public Vector3 StartPosition;

    public Vector3 GetRandomPoint()
    {
        var maxIndex = WayPoints.Count;
        return WayPoints[Random.Range(0, maxIndex)];
    }
}
