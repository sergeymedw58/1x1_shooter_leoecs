using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct BulletComponent
{
    public Transform Transform;
    public int Damage;    
}
