using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leopotam.Ecs;
using TMPro;

public class GameSettings
{
    public GameSettings(Bullet bullet, PlayerSettings playerSettings, EnemySettings enemySettings)
    {
        Bullet = bullet;
        PlayerSettings = playerSettings;
        EnemySettings = enemySettings;
    }
    public Bullet Bullet { get; }
    public PlayerSettings PlayerSettings { get; }

    public EnemySettings EnemySettings { get; }
}

public class ScoreView
{
    public TextMeshProUGUI PlayerScore;
    public TextMeshProUGUI EnemyScore;
}

public class GameStartUp : MonoBehaviour
{
    private EcsWorld _world;
    private EcsSystems _initSystems;
    private EcsSystems _updateSystems;
    [SerializeField]
    private Bullet _bullet;
    [SerializeField]
    private PlayerSettings _playerSettings;
    [SerializeField]
    private EnemySettings _enemyEnemySettings;
    [SerializeField]
    private TextMeshProUGUI _playerScore;
    [SerializeField]
    private TextMeshProUGUI _enemyScore;


    private void Start()
    {
        GameSettings settings = new GameSettings(_bullet, _playerSettings, _enemyEnemySettings);

        ScoreView scoreview = new ScoreView();
        scoreview.PlayerScore = _playerScore;
        scoreview.EnemyScore = _enemyScore;

        _world = new EcsWorld();

        _initSystems = new EcsSystems(_world)
            .Add(new PlayerInitSystem())
            .Add(new EnemyInitSystem())
            .Add(new ShootInitSystem())
            .Inject(settings);


        _updateSystems = new EcsSystems(_world)
            .Add(new PlayerInputSystem())
            .Add(new PlayerMovementSystem())
            .Add(new EnemyMovementSystem())
            .Add(new EnemyAimingSystem())
            .Add(new ShootSystem())
            .Add(new BulletMovementSystem())
            .Add(new BulletCollisionSystem())
            .Add(new DamageSystem())
            .Add(new ScoreViewSystem())
            .Inject(scoreview)
            .OneFrame<CollisionInfoComponent>()
            .OneFrame<DamagedComponent>();


        _initSystems.ProcessInjects();
        _updateSystems.ProcessInjects();

        _initSystems.Init();
        _updateSystems.Init();
    }

    private void Update()
    {
        _updateSystems.Run();
    }
}
