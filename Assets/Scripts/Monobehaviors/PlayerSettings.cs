using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Leopotam.Ecs;

public class PlayerSettings : MonoBehaviour
{
    public NavMeshAgent Agent;
    public int Health;
    public float Speed;
    public Transform Transform;
    public Transform ShootPoint;
}
