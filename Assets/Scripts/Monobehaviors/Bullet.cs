using UnityEngine;
using Leopotam.Ecs;
using System.Collections;
using System;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    private float _speed;
    [SerializeField]
    private int _damage;

    private EcsEntity _bulletEntity;

    public void Initialize(Vector3 direction, EcsWorld world)
    {
        transform.forward = direction;
        _bulletEntity =  world.NewEntity();
        ref var movement = ref _bulletEntity.Get<MovementComponent>();
        ref var bullet = ref _bulletEntity.Get<BulletComponent>();

        movement.Speed = _speed;
        movement.Direction = direction;
        bullet.Transform = transform;
        bullet.Damage = _damage;
    }

    public void DestroyBullet()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        ref var hit = ref _bulletEntity.Get<CollisionInfoComponent>();
        hit.Other = collision.gameObject;
        hit.Normal = collision.contacts[0].normal;
        hit.Bullet = this;
    }
}
