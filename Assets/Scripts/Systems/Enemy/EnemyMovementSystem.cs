using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leopotam.Ecs;

public class EnemyMovementSystem : IEcsRunSystem
{
    private EcsFilter<EnemyComponent, MovementComponent> _filter;

    public void Run()
    {
        foreach(var i in _filter)
        {
            ref var enemy = ref _filter.Get1(i);
            ref var movement = ref _filter.Get2(i);
            if(!enemy.IsOnTheWay)
            {
                enemy.CanAiming = false;
                var nextPopint = enemy.GetRandomPoint();
                if(movement.Direction != nextPopint)
                {
                    movement.Direction = nextPopint;
                    enemy.Agent.SetDestination(movement.Direction);
                    enemy.IsOnTheWay = true;
                }
            }
            var x = enemy.Transform.position.x;
            var z = enemy.Transform.position.z;
            var xDestination = movement.Direction.x;
            var zDestination = movement.Direction.z;
            var ispointArrived = x == xDestination && z == zDestination;
            if(ispointArrived && enemy.IsOnTheWay)
            {
                enemy.IsOnTheWay = false;
                enemy.CanAiming = true;
            }
        }
    }
}
