using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leopotam.Ecs;
using UnityEngine.AI;

public class EnemyInitSystem : IEcsInitSystem
{
    private EcsWorld _world;
    private GameSettings _settings;

    public void Init()
    {
        var enemyEntity = _world.NewEntity();
        ref var enemy = ref enemyEntity.Get<EnemyComponent>();
        ref var movement = ref enemyEntity.Get<MovementComponent>();
        ref var shoot = ref enemyEntity.Get<ShootComponent>();
        ref var health = ref enemyEntity.Get<HealthComponent>();

        shoot.IsShooting = false;

        movement.Speed = _settings.EnemySettings.Speed;
        enemy.WayPoints = _settings.EnemySettings.WayPoints;
        health.Health = _settings.EnemySettings.Health;
        enemy.ShootPoint = _settings.EnemySettings.ShootPoint;
        enemy.Agent = _settings.EnemySettings.Agent;
        enemy.Agent.speed = movement.Speed;
        enemy.Transform = _settings.EnemySettings.Transform;
        enemy.StartPosition = enemy.Transform.position;
        enemy.CanAiming = false;
        enemy.IsOnTheWay = false;
        enemy.Score = 0;
    }
}
