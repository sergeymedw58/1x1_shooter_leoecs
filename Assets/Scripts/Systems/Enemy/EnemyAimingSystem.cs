using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leopotam.Ecs;

public class EnemyAimingSystem : IEcsRunSystem
{
    private EcsFilter<EnemyComponent, ShootComponent> _enemyFilter;
    private EcsFilter<PlayerComponent> _playerFilter;
    public void Run()
    {
        foreach(var i in _enemyFilter)
        {
            ref var enemy = ref _enemyFilter.Get1(i);
            ref var shoot = ref _enemyFilter.Get2(i);

            if(enemy.CanAiming)
            {               
                foreach(var j in _playerFilter)
                {
                    ref var player = ref _playerFilter.Get1(j);
                    enemy.Transform.LookAt(player.Transform.position);
                }
                shoot.Direction = enemy.Transform.forward;
                shoot.BulletSpawnPoint = enemy.ShootPoint.position;
                shoot.IsShooting = true;
            }
        }
    }
}
