using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leopotam.Ecs;

public class DamageSystem : IEcsRunSystem
{
    private EcsFilter<HealthComponent, DamagedComponent> _filter;
    private EcsFilter<PlayerComponent> _playerFilter;
    private EcsFilter<EnemyComponent> _enemyFilter;

    public void Run()
    {
        foreach(var i in _filter)
        {
            ref var health = ref _filter.Get1(i);
            ref var damage = ref _filter.Get2(i);

            health.Health -= damage.Damage;
            if(health.Health <= 0)
            {
                ref var entity = ref _filter.GetEntity(i);
                if(entity.Has<PlayerComponent>())
                {
                    ref var player = ref entity.Get<PlayerComponent>();
                    player.Score++;
                    player.Transform.position = player.StartPosition;
                    foreach (var j in _enemyFilter)
                    {
                        ref var enemy = ref _enemyFilter.Get1(i);
                        enemy.Transform.position = enemy.StartPosition;
                    }
                }
                else if(entity.Has<EnemyComponent>())
                {
                    ref var enemy = ref entity.Get<EnemyComponent>();
                    enemy.Score++;
                    enemy.Transform.position = enemy.StartPosition;
                    foreach (var j in _enemyFilter)
                    {
                        ref var player = ref _playerFilter.Get1(i);
                        player.Transform.position = player.StartPosition;
                    }
                }
            }
        }
    }
}
