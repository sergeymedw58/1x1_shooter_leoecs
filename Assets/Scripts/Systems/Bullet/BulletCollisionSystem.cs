using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leopotam.Ecs;

public class BulletCollisionSystem : IEcsRunSystem
{
    private EcsFilter<CollisionInfoComponent, MovementComponent, BulletComponent> _filter;
    private EcsFilter<PlayerComponent> _playerFliter;
    private EcsFilter<EnemyComponent> _enemyFilter;
    public void Run()
    {
        foreach(var i in _filter)
        {
            ref var collisonInfo = ref _filter.Get1(i);
            ref var movement = ref _filter.Get2(i);
            ref var bullet = ref _filter.Get3(i);
            if(collisonInfo.Other.CompareTag("Wall"))
            {
                movement.Direction = Vector3.Reflect(movement.Direction, collisonInfo.Normal);
            }
            if(collisonInfo.Other.CompareTag("Border"))
            {
                ref var bulletEntity = ref _filter.GetEntity(i);
                collisonInfo.Bullet.DestroyBullet();
                bulletEntity.Destroy();               
            }
            else if(collisonInfo.Other.CompareTag("Player"))
            {
                foreach(var j in _playerFliter)
                {
                    ref var playerEntity = ref _playerFliter.GetEntity(j);
                    ref var damaged = ref playerEntity.Get<DamagedComponent>();
                    damaged.Damage = bullet.Damage;
                }
            }
            else if(collisonInfo.Other.CompareTag("Enemy"))
            {
                foreach (var j in _enemyFilter)
                {
                    ref var enemyEntity = ref _enemyFilter.GetEntity(j);
                    ref var damaged = ref enemyEntity.Get<DamagedComponent>();
                    damaged.Damage = bullet.Damage;
                }
            }
        }
    }
}
