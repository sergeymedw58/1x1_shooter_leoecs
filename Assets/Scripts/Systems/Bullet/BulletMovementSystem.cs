using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leopotam.Ecs;
public class BulletMovementSystem : IEcsRunSystem
{
    private EcsFilter<BulletComponent, MovementComponent> _filter;
    public void Run()
    {
        foreach(var i in _filter)
        {
            ref var bullet = ref _filter.Get1(i);
            ref var movment = ref _filter.Get2(i);
            bullet.Transform.position += movment.Direction * movment.Speed * Time.deltaTime;
        }
    }
}