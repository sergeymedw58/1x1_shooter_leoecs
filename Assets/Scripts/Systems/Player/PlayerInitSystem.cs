using Leopotam.Ecs;
using UnityEngine;
using UnityEngine.AI;

public class PlayerInitSystem : IEcsInitSystem
{
    private EcsWorld _world;
    private GameSettings _settings;
    public void Init()
    {
        var playerEntity = _world.NewEntity();
        ref var player = ref playerEntity.Get<PlayerComponent>();
        ref var movement = ref playerEntity.Get<MovementComponent>();
        ref var shoot = ref playerEntity.Get<ShootComponent>();
        ref var health = ref playerEntity.Get<HealthComponent>();
        
        shoot.IsShooting = false;
        movement.Speed = _settings.PlayerSettings.Speed;
        health.Health = _settings.PlayerSettings.Health;
        player.ShootPoint = _settings.PlayerSettings.ShootPoint;
        player.Agent = _settings.PlayerSettings.Agent;
        player.Transform = _settings.PlayerSettings.Transform;
        player.StartPosition = player.Transform.position;
        player.Score = 0;
    }
}