using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leopotam.Ecs;
public class PlayerMovementSystem : IEcsRunSystem
{
    private EcsFilter<PlayerComponent, MovementComponent> _filter;
    public void Run()
    {
        foreach(var i in _filter)
        {
            ref var player = ref _filter.Get1(i);
            ref var movement = ref _filter.Get2(i);
            player.Agent.Move(movement.Direction * movement.Speed * Time.deltaTime);
            player.Transform.LookAt(player.Transform.position + movement.Direction);
        }
    }
}
