using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leopotam.Ecs;

public class PlayerInputSystem : IEcsRunSystem
{
    private EcsFilter<MovementComponent, ShootComponent, PlayerComponent> _filter;
    public void Run()
    {
        float z = Input.GetAxis("Vertical");
        float x = Input.GetAxis("Horizontal");

        foreach(var i in _filter)
        {
            ref var movement = ref _filter.Get1(i);
            ref var shoot = ref _filter.Get2(i);
            ref var player = ref _filter.Get3(i);

            movement.Direction = new Vector3(x, 0, z);
            if(Input.GetKeyDown(KeyCode.Space))
            {
                shoot.IsShooting = true;
                shoot.BulletSpawnPoint = player.ShootPoint.position;
                shoot.Direction = player.Transform.forward;
            }
        }
    }

}
