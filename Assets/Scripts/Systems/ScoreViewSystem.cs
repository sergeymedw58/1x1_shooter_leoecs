using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leopotam.Ecs;

public class ScoreViewSystem : IEcsRunSystem
{
    private ScoreView _scoreViev;

    private EcsFilter<PlayerComponent> _playerFilter;
    private EcsFilter<EnemyComponent> _enemyFilter;

    public void Run()
    {
        foreach(var i in _playerFilter)
        {
            ref var player = ref _playerFilter.Get1(i);
            _scoreViev.PlayerScore.text = $"{player.Score}";
        }
        foreach (var i in _enemyFilter)
        {
            ref var enemy = ref _enemyFilter.Get1(i);
            _scoreViev.EnemyScore.text = $"{enemy.Score}";
        }
    }
}
