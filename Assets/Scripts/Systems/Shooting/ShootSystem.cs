using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leopotam.Ecs;

public class ShootSystem : IEcsRunSystem
{
    private EcsWorld _world;
    private EcsFilter<ShootComponent> _filter;

    public void Run()
    {
        foreach(var i in _filter)
        {
            ref var shootComponent = ref _filter.Get1(i);
            if(shootComponent.IsShooting)
            {
                var bullet = GameObject.Instantiate(shootComponent.Bullet, shootComponent.BulletSpawnPoint, Quaternion.identity);
                bullet.Initialize(shootComponent.Direction, _world);
                shootComponent.IsShooting = false;                
            }
        }
    }   
}