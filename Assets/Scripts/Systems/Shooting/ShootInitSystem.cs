using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Leopotam.Ecs;

public class ShootInitSystem : IEcsInitSystem
{
    private EcsFilter<ShootComponent> _filter;
    private GameSettings _settings;

    public void Init()
    {
        foreach(var i in _filter)
        {
            ref var shootComponent = ref _filter.Get1(i);
            shootComponent.Bullet = _settings.Bullet;
        }
    }
}
